
function App() {
  const gUserInfo = {
    firstname: 'Hoang',
    lastname: 'Pham',
    avatar: 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
    age: 30,
    language: ['Vietnamese', 'Japanese', 'English']
  }
  
  return (
    <div className="user-infor">
      <h5>Họ và Tên: {gUserInfo.firstname} {gUserInfo.lastname}</h5>
      <img src={gUserInfo.avatar} width={300} alt="logo"/>
      <p>Tuổi của user là {gUserInfo.age}</p>
      <p> {gUserInfo.age< 35?"Anh ấy còn trẻ":"Anh ấy đã già"}</p>
      <ul>Ngoại ngữ
       {gUserInfo.language.map(function(element, index){
        return <li key={index}>{element}</li>
       })}
      </ul>
    </div>
  );
}

export default App;
